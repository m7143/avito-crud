package org.example.manager;

import org.example.data.Flat;
import org.example.exception.FlatNotFoundException;
import org.example.request.SearchRequest;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class FlatManagerTest {

  FlatManager flatManager = new FlatManager();
  static Flat flat1 = new Flat(1, "2", 2100000, 40, true, false, 4, 5, false);
  static Flat flat2 = new Flat(2, "3", 3600000, 60, false, true, 5, 9, false);
  static Flat flat3 = new Flat(3, "studio", 1500000, 32, true, false, 7, 19, false);

  static Flat flatNotFirst = new Flat(1, "1", 1200000, 30, true, false, 1, 5, false);
  static Flat flatNotFirst2 = new Flat(2, "3", 2060000, 80, true, false, 5, 5, false);

  static SearchRequest request = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 150,
          true, false, 2, 5, 2, 20, false, false);
  static SearchRequest requestFlatNotFirst = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 150,
          true, false, 2, 5, 2, 20, false, false);


  @Test
  void shoulCreate() {
    List<Flat> expectedAll = new ArrayList<>();
    List<Flat> actualAll = flatManager.getAll();
    assertEquals(expectedAll, actualAll);
  }

  @Test
  void shouldByIdNull() {
    flatManager.create(flat2);
    assertThrows(FlatNotFoundException.class, () -> flatManager.getById(0));
  }



  @Test
  void shouldAddSingle() {
    int notExpectedId = 1;
    Flat actualCreate = flatManager.create(flat1);
    assertNotEquals(notExpectedId, actualCreate.getId());

    List<Flat> expectedAll = new ArrayList<>();
    expectedAll.add(flat1);
    List<Flat> actualAll = flatManager.getAll();
    assertEquals(expectedAll, actualAll);

    Flat actualById = flatManager.getById(actualCreate.getId());
    assertNotNull(actualById);
  }

  @Test
  void shouldAddMultiple() {
    int notExpectedId = 10;
    Flat actualCreate = flatManager.create(flat1);
    flatManager.create(flat2);
    assertNotEquals(notExpectedId, actualCreate.getId());

    List<Flat> expectedAll = new ArrayList<>();
    expectedAll.add(flat1);
    expectedAll.add(flat2);

    List<Flat> actualAll = flatManager.getAll();
    assertEquals(expectedAll, actualAll);

    Flat actualById = flatManager.getById(actualCreate.getId());
    assertNotNull(actualById);

  }

  @Test
  void shouldSearchNull() {
    SearchRequest dtoFlatNull = new SearchRequest(null, null, null, null, null,
            true, false, null, null, null, null, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(dtoFlatNull);
    assertEquals(expected, actual);
  }

  @Test
  void shouldSearchNoResult() {
    List<Flat> actual = flatManager.searchBy(new SearchRequest());
    assertEquals(0, actual.size());
  }

  @Test
  void shouldSearchSingleResult() {
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(request);
    assertEquals(expected, actual);
  }

  @Test
  void shouldSearchSingleResultArray() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2", "3"}, 1000000, 5000000, 20, 150,
            true, false, 2, 5, 2, 20, false, false);
    Flat flat2 = new Flat(4, "studio", 1200000, 40, true, false, 4, 5, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    flatManager.create(flat2);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertEquals(expected, actual);
  }

  @Test
  void shouldSearchSingleResultFlatRoomFreeStyle() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"free_style"}, 1000000, 5000000, 20, 150,
            true, false, 2, 5, 2, 20, false, false);
    Flat flat1 = new Flat(5, "free_style", 2100000, 30, true, false, 4, 5, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertEquals(expected, actual);
  }

  @Test
  void shouldSearchMultiResultFiveAndMoreFive() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"5"}, 1000000, 5000000, 20, 150,
            true, false, 2, 5, 2, 20, false, false);

    Flat flat1 = new Flat(6, "6", 3000000, 110, true, false, 4, 5, false);
    Flat flat2 = new Flat(7, "7", 4300000, 140, true, false, 4, 5, false);
    Flat flat3 = new Flat(8, "free_style", 1100000, 35, true, false, 4, 5, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);
    expected.add(flat2);
    expected.add(flat3);

    flatManager.create(flat1);
    flatManager.create(flat2);
    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldSearchMultiResultFiveAndMore() {
    SearchRequest dtoFlat = new SearchRequest(new String[]{"5", "studio", "free_style"}, 1000000, 5000000, 20, 150,
            true, false, 2, 5, 2, 20, false, false);
    Flat flat1 = new Flat(3, "6", 2200000, 120, true, false, 4, 5, false);
    Flat flat2 = new Flat(3, "studio", 1400000, 30, true, false, 4, 5, false);
    Flat flat3 = new Flat(3, "free_style", 1400000, 30, true, false, 4, 5, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);
    expected.add(flat2);
    expected.add(flat3);

    flatManager.create(flat1);
    flatManager.create(flat2);
    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(dtoFlat);
    assertEquals(expected, actual);
  }

  @Test
  void shouldSearchMultiResultFiveAndMoreResult() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"6", "studio", "free_style"}, 1000000, 5_000_000, 20, 150,
            true, false, 2, 5, 2, 20, false, false);
    Flat flat1 = new Flat(4, "3", 24000000, 100, true, false, 4, 5, false);
    Flat flat2 = new Flat(5, "studio", 24000000, 40, true, false, 4, 5, false);
    Flat flat3 = new Flat(6, "free_style", 24000000, 70, true, false, 4, 5, false);
    Flat flat4 = new Flat(7, "6", 24000000, 80, true, false, 4, 5, false);
    Flat flat5 = new Flat(8, "7", 24000000, 40, true, false, 4, 5, false);
    Flat flat6 = new Flat(9, "8", 24000000, 55, true, false, 4, 5, false);

    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);
    expected.add(flat2);
    expected.add(flat3);
    expected.add(flat4);
    expected.add(flat5);
    expected.add(flat6);

    flatManager.create(flat1);
    flatManager.create(flat2);
    flatManager.create(flat3);
    flatManager.create(flat4);
    flatManager.create(flat5);
    flatManager.create(flat6);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldAddNotTheLast() {
    List<Flat> expected = new ArrayList<>();
    expected.add(flatNotFirst);
    expected.add(flatNotFirst2);

    flatManager.create(flatNotFirst);
    flatManager.create(flatNotFirst2);
    List<Flat> actual = flatManager.searchBy(requestFlatNotFirst);
    assertNotEquals(expected, actual);

  }

  @Test
  void shouldSearchMultiplyResult() {
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);
    expected.add(flat2);
    expected.add(flat3);

    flatManager.create(flat1);
    flatManager.create(flat2);
    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(request);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldUpdate() {
    Flat expected = flat1;

    flatManager.create(flat1);
    Flat actual = flatManager.update(flat1);
    assertEquals(expected, actual);
  }

  @Test
  void shouldUpdateNull() {
    assertThrows(FlatNotFoundException.class, () -> flatManager.update(flat1));
  }

  @Test
  void shouldRemoveId() {
    flatManager.create(flat1);
    boolean actualRemove = flatManager.removeById(flat1.getId());
    assertTrue(actualRemove);

    long expectedCount = flatManager.getCount();
    long actualCount = flatManager.getCount();
    assertEquals(expectedCount, actualCount);
  }

  @Test
  void shouldRemoveNotExistent() {
    flatManager.create(flat1);
    long expectedCount = flatManager.getCount();


    boolean actualRemove = flatManager.removeById(7);
    assertFalse(actualRemove);

    long actualCount = flatManager.getCount();
    assertEquals(expectedCount, actualCount);
  }

  @Test
  void shouldRemoveExistent() {
    flatManager.create(flat1);
    long expectedCount = flatManager.getCount() - 1;

    boolean actualRemove = flatManager.removeById(flat1.getId());
    assertTrue(actualRemove);

    long actualCount = flatManager.getCount();
    assertEquals(expectedCount, actualCount);
  }

  @Test
  void shouldSearchMultiResultNotOneResult() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"5", "studio", "free_style"}, 1000000, 5000000, 20, 150,
            true, false, 2, 5, 2, 20, false, false);
    Flat flat1 = new Flat(4, "2", 1700000, 43, true, false, 4, 5, false);
    Flat flat2 = new Flat(5, "studio", 1700000, 43, true, false, 4, 5, false);
    Flat flat3 = new Flat(6, "free_style", 1700000, 43, true, false, 4, 5, false);
    Flat flat4 = new Flat(7, "6", 1700000, 43, true, false, 4, 5, false);
    Flat flat5 = new Flat(8, "7", 1700000, 43, true, false, 4, 5, false);
    Flat flat6 = new Flat(9, "8", 1700000, 43, true, false, 4, 5, false);

    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);
    expected.add(flat2);
    expected.add(flat3);
    expected.add(flat4);
    expected.add(flat5);
    expected.add(flat6);

    flatManager.create(flat1);
    flatManager.create(flat2);
    flatManager.create(flat3);
    flatManager.create(flat4);
    flatManager.create(flat5);
    flatManager.create(flat6);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldMinPriceMorePrice() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 3000000, 5000000, 20, 150,
            true, false, 2, 5, 2, 20, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldMaxPriceLessPrice() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 1900000, 20, 150,
            true, false, 2, 5, 2, 20, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldMinSquareMoreSquare() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 90, 100,
            true, false, 2, 5, 2, 20, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldMaxSquareLessSquare() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 19,
            true, false, 2, 5, 2, 20, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldNotBalcony() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 150,
            false, false, 2, 5, 2, 20, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldNotLogy() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 150,
            true, true, 2, 5, 2, 20, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldMinFloorMoreFloor() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"studio"}, 1000000, 5000000, 20, 150,
            true, false, 4, 5, 2, 20, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat3);

    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldMinFloorMoreFloor1() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"studio"}, 1000000, 5000000, 20, 150,
            true, false, 8, 9, 2, 20, false, false);
    List<Flat> expected = new ArrayList<>();

    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertEquals(expected, actual);
  }

  @Test
  void shouldMaxFloorLessFloor() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1_000_000, 5_900_000, 20, 100,
            true, false, 2, 2, 2, 19, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat3);

    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldNotTheFirst() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 100,
            true, false, 1, 5, 1, 19, true, false);
    Flat flat3 = new Flat(6, "2", 2000000, 32, true, false, 1, 19, false);

    List<Flat> expected = new ArrayList<>();
    expected.add(flat3);

    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldNotTheFirst2() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 100,
            true, false, 1, 5, 1, 19, true, false);
    Flat flat3 = new Flat(6, "2", 2000000, 32, true, false, 2, 19, false);

    List<Flat> expected = new ArrayList<>();
    expected.add(flat3);

    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertEquals(expected, actual);
  }

  @Test
  void shouldNotTheLast() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 100,
            true, false, 2, 20, 2, 20, false, true);
    Flat flat3 = new Flat(6, "2", 2000000, 32, true, false, 19, 19, false);

    List<Flat> expected = new ArrayList<>();
    expected.add(flat3);

    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldNotTheLast2() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5000000, 20, 100,
            true, false, 2, 20, 2, 20, false, true);
    Flat flat3 = new Flat(6, "2", 2_000_000, 32, true, false, 2, 19, false);

    List<Flat> expected = new ArrayList<>();
    expected.add(flat3);

    flatManager.create(flat3);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertEquals(expected, actual);
  }

  @Test
  void shouldMinTotalFloorMoreTotalFloor() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 2000000, 5000000, 20, 100,
            true, false, 2, 5, 6, 1, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldMaxTotalFloorLessTotalFloor() {
    SearchRequest searchRequest = new SearchRequest(new String[]{"2"}, 1000000, 5900000, 20, 100,
            true, false, 2, 5, 2, 4, false, false);
    List<Flat> expected = new ArrayList<>();
    expected.add(flat1);

    flatManager.create(flat1);
    List<Flat> actual = flatManager.searchBy(searchRequest);
    assertNotEquals(expected, actual);
  }

  @Test
  void shouldAddNotTheFirst() {
    List<Flat> expected = new ArrayList<>();
    expected.add(flatNotFirst);
    expected.add(flatNotFirst2);

    flatManager.create(flatNotFirst);
    flatManager.create(flatNotFirst2);
    List<Flat> actual = flatManager.searchBy(requestFlatNotFirst);
    assertNotEquals(expected, actual);

  }

  @Test
  void checkGetIndexByID() {
    List<Flat> expected = new ArrayList<>();
    flatManager.create(flat1);
    flatManager.create(flat2);
    flatManager.create(flat3);

    flatManager.update(flat1);
    flatManager.update(flat2);
    flatManager.update(flat3);

    expected.add(flat1);
    expected.add(flat2);
    expected.add(flat3);



    List<Flat> actual = flatManager.getAll();
    assertEquals(expected, actual);
  }

}
