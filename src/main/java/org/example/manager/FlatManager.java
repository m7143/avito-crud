package org.example.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.data.Flat;
import org.example.exception.FlatNotFoundException;
import org.example.request.SearchRequest;


import java.util.ArrayList;
import java.util.List;

@Slf4j
public class FlatManager {
    private static final int SEARCH_SIZE_LIMIT = 5;
    private final List<Flat> flats = new ArrayList<>(50);
    static final String STUDIO = "studio";
    static final String FREE_STYLE = "free_style";
    static private long count = 0;

    // CRUD
    public List<Flat> getAll() {
        return new ArrayList<>(flats);
    }

    public Flat getById(final long id) {
        log.debug("getById Id, Id: {}", id);
        for (final Flat flat : flats) {
            if (flat.getId() == id) {
                return flat;
            }
        }
        throw new FlatNotFoundException("param with id: " + id + "not found" + "\n");
    }

    public List<Flat> searchBy(final SearchRequest search) {
        log.debug("Search search, search: {}", search + "\n");
        final List<Flat> results = new ArrayList<>(SEARCH_SIZE_LIMIT);
        for (Flat flat : flats) {
            if (search.getRooms() != null) {
                final String room = maxRooms(search, flat);
                if (room.equals("")) {
                    continue;
                }
            }
            if (search.getMinPrice() != null) {
                if (!(search.getMinPrice() <= flat.getPrice())) {
                    continue;
                }

            }
            if (search.getMaxPrice() != null) {
                if (!(flat.getPrice() <= search.getMaxPrice())) {
                    continue;
                }
            }
            if (search.getMinSquare() != null) {
                if (!(search.getMinSquare() <= flat.getSquare())) {
                    continue;
                }
            }
            if (search.getMaxSquare() != null) {
                if (!(flat.getSquare() <= search.getMaxSquare())) {
                    continue;
                }
            }
            if (!(flat.isBalcony() == search.isBalcony())) {
                continue;
            }
            if (!(flat.isLoggia() == search.isLoggia())) {
                continue;
            }
            if (search.getMinFloor() != null) {
                if (!(search.getMinFloor() <= flat.getFloor())) {
                    continue;
                }
            }
            if (search.getMaxFloor() != null) {
                if (!(flat.getFloor() <= search.getMaxFloor())) {
                    continue;
                }
            }
            if (search.isNotTheFirst()) {
                if (flat.getFloor() == 1) {
                    continue;
                }
            }
            if (search.isNotTheLast()) {
                if (flat.getFloor() == flat.getFloorInTheHouse()) {
                    continue;
                }
            }
            if (search.getMinTotalFloorInTheHouse() != null) {
                if (!(search.getMinTotalFloorInTheHouse() <= flat.getFloorInTheHouse())) {
                    continue;
                }
            }
            if (search.getMaxTotalFloorInTheHouse() != null) {
                if (!(flat.getFloorInTheHouse() <= search.getMaxTotalFloorInTheHouse())) {
                    continue;
                }
            }
            results.add(flat);

            if (results.size() >= SEARCH_SIZE_LIMIT) {
                return results;
            }
        }

        return results;
    }

    public Flat create(final Flat flat) {
        log.debug("create flat, flat: {}", flat.toString());
        flats.add(flat);
        count++;
        flat.setId(count);
        return flat;
    }

    public Flat update(final Flat flat) {
        log.debug("update flat, flat: {}", flat + "\n");
        final int index = getIndexById(flat.getId());
        if (index == -1) {
            throw new FlatNotFoundException("param with id: " + flat.getId() + " not found");
        }
        flats.set(index, flat);
        return flat;
    }

    public boolean removeById(final long id) {
        log.debug("removeById id, id: {}", id + "\n");
        final boolean removed = flats.removeIf(o -> o.getId() == id);
        if (!removed) {
            return false;
        }
        return true;
    }

    public int getCount() {
        log.debug("getCount size, size: {}", flats.size());
        return flats.size();
    }

    private int getIndexById(final long id) {
        for (int i = 0; i < flats.size(); i++) {
            final Flat flat = flats.get(i);
            if (flat.getId() == id) {
                return i;
            }
        }
        return -1;
    }

    private String maxRooms(final SearchRequest dtoFlat, final Flat flat) {
        log.debug("TestMaxRooms dtoFlat, dtoFlat: {}, flat: {}", dtoFlat + "\n", flat + "\n");
        final String maxFlatMoreFive = "5";
        final String notResult = "";
        for (String room : dtoFlat.getRooms()) {
            if (room.equals(flat.getRooms())) {
                return room;
            }
            if (room.equals(maxFlatMoreFive)) {

                if (flat.getRooms().equals(STUDIO)) {
                    continue;
                }
                if (flat.getRooms().equals(FREE_STYLE)) {
                    continue;
                }
                if (!(Integer.parseInt(flat.getRooms()) >= Integer.parseInt(maxFlatMoreFive))) {
                    continue;
                }
                return room;
            }
        }
        return notResult;
    }

}
