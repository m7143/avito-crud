package org.example.exception;

public class FlatNotFoundException extends RuntimeException {
    public FlatNotFoundException() {
        super();
    }

    public FlatNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FlatNotFoundException(Throwable cause) {
        super(cause);
    }

    protected FlatNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public FlatNotFoundException(String message) {
        super(message);
    }


}
